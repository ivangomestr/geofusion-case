import { Header } from "./components/Header/Header"
import { Home } from "./components/Home/Home"
import { GlobalStyle } from "./styles/global"

export const App = () => {
    return (
        <>
          <Header />
          <Home />
          <GlobalStyle />
        </>
    )
}
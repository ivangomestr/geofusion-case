import { createGlobalStyle } from 'styled-components'

export const GlobalStyle = createGlobalStyle`
   :root {
       --background: #ECF0F0;
       --blue: #0080B2;
       --red: #BC5D5D;
       --white: #FFFFFF;
       --text-body: #434343;
   }

   * {
       margin: 0;
       padding: 0;
       box-sizing: border-box;

}

   html { 
        @media (max-width: 1080px) {
            font-size: 93.75% //15px
        }

        @media (max-width: 720px) {
            font-size: 87.5% // 14px
        }
    }

    body {
      background: var(--background);
      -webkit-font-smoothing: antialiased;
    }
`
import { Container } from "./styles";
import { MapContainer, TileLayer, Marker, Popup } from 'react-leaflet';
import data from "../../data/data.json"
import { Icon } from "leaflet"
import markerRed from "../../assets/image/marker-red.png"


export const Maps = () => {
    const icon = new Icon({
        iconUrl: markerRed,
        iconSize: [20, 20]
    })

    const position = [-23.568767, -46.649907]

    const marker = data.stores.map((store) => {
        return (
            <Marker
                position={[store.latitude, store.longitude]}
                icon={icon}
            >
                <Popup>
                    {store.name}
                </Popup>
            </Marker>
        )
    })


    return (
        <Container>
            <MapContainer center={position} zoom={13}>
                <TileLayer
                    attribution='&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
                    url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
                />
                {marker}
            </MapContainer>
        </Container>
    )
}

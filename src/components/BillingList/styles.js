import styled from 'styled-components'

export const Container = styled.div`
   border: 1px solid black;
   width: 26vw;
   height: 45vh;

   table {
       width: 100%;
       height: 100%;
       border-spacing: 0 0.5rem;
       border-collapse: collapse;
   }

    th, td {
       padding: 10px;
   }

   tr {
       border: 1px solid black;
   }

    th:nth-child(1) {
    text-align: left;
   }

   td:nth-child(1) {
    text-align: left;
   }

   
    td, th:nth-child(2) {
    text-align: right;
   }
`
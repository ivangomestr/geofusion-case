import styled from "styled-components"

export const Container = styled.header`
   background: var(--blue);
   height: 7vh;
   display: flex;
   align-items: center;
   padding: 30px;

   h2 {
       color: var(--white)
   }
`
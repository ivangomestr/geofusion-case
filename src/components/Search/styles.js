import styled from 'styled-components'

export const Container = styled.div`
   border: 2px solid black;
   width: 40vw;
   height: 7vh;
   margin: 40px;
   display: flex;
   justify-content: space-evenly;
   align-items: center;

   input {
       background: var(--white);
       width: 30%;
       height: 30px;
       border: var(--background);
       border-radius: 7px;
   }
`
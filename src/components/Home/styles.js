import styled from 'styled-components'

export const Container = styled.div`
   display: flex;
   width: 100%;
   height: 100%;
   flex-direction: column;
   justify-content: center;
   align-items: center;
`

export const Content = styled.div`
   border: 1px solid red;
   width: 70vw;
   height: 48vh;
   display: flex;
   justify-content: space-evenly;
`
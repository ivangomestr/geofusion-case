import { BillingList } from "../BillingList/BillingList";
import { Maps } from "../Maps/Maps";
import { Search } from "../Search/Search";
import { Container, Content } from "./styles";

export const Home = () => {
    return (
        <Container>
                <Search />
                
            <Content>
                <BillingList />
                <Maps />
            </Content>
        </Container>
    );
};
